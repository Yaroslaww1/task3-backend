import { post, get } from "../requestHelper"
const entity = 'fights'

export const createFight = async (body) => {
    console.log(body)
    let data = await post(entity, body)
    return data
}

export const getAllFights = async () => {
    let data = await get(entity)
    return data
}