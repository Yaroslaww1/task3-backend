import { controls } from '../constants/fightKeys'

const players = {
    FIRST: 0,
    SECOND: 1,
}
  
function compareArray(_array1, _array2) {
    let array1 = _array1.slice().sort();
    let array2 = _array2.slice().sort();
    return array1.length === array2.length && array1.every((value, index) => value === array2[index]);
}
  
class Player {
  
    __isBlocking = false;
  
    constructor(fighter) {
      this.fighter = fighter;
      this.keysPressed = [];
      this.health = fighter.health;
      this.lastCriticalTime = 0;
  
      this.canDealCritical = this.canDealCritical.bind(this);
      this.criticalDealed = this.criticalDealed.bind(this);
    }
  
    receiveDamage(damage) {
      this.health -= damage;
    }
  
    isAlive() {
      return this.health > 0.0 ? true : false;
    }
  
    handleKeyPress(keyCode) {
      this.keysPressed.push(keyCode);
    //   console.log(this.keysPressed);
      if (this.keysPressed.length >= 4)
        this.keysPressed = this.keysPressed.slice(1, 5);
    }
  
    canDealCritical(time, criticalComboKeysCombination) {
    //   console.log(this.keysPressed, criticalComboKeysCombination, compareArray(this.keysPressed, criticalComboKeysCombination))
      if (!compareArray(this.keysPressed, criticalComboKeysCombination))
        return false;
      if (Math.abs(time - this.lastCriticalTime) >= 10 * 1000) {
        return true;
      } else {
        return false;
      }
    }
  
    criticalDealed(time) {
      this.lastCriticalTime = time;
    }
  
    getHealthInPercent() {
      return Math.floor(this.health * 100 / this.fighter.health);
    }
  
    setBlocking(newBlocking) {
      this.__isBlocking = newBlocking;
    }
  
    isBlocking() {
      return this.__isBlocking;
    } 
}

class Fight {

    log = []

    constructor(fighter1, fighter2, updateIndicatorsHandler, fightFinishedHandler) {
        this.fighter1 = fighter1
        this.fighter2 = fighter2

        this.player1 = new Player(fighter1)
        this.player2 = new Player(fighter2)

        this.updateIndicatorsHandler = updateIndicatorsHandler
        this.fightFinishedHandler = fightFinishedHandler

        this.handleKeyup = this.handleKeyup.bind(this)
        this.handleKeydown = this.handleKeydown.bind(this)
    }

    handleKeyup(code) {
        code = code.code
        if (code === controls.PlayerOneBlock) {
            this.player1.setBlocking(false);
        }
        if (code === controls.PlayerTwoBlock) {
            this.player2.setBlocking(false);
        }
    }

    handleKeydown(code) {
        code = code.code
        let damage = 0;

        let { fighter1: firstFighter, fighter2: secondFighter } = this

        switch (decidePlayerByButton(code)) {
            case players.FIRST: {
                this.player1.handleKeyPress(code);

                if (code === controls.PlayerOneBlock) {
                    this.player1.setBlocking(true);
                    break;
                }

                if (code === controls.PlayerOneAttack && this.player1.isBlocking() === false) 
                    damage = getDamage(firstFighter, secondFighter);

                if (this.player2.isBlocking(controls.PlayerTwoBlock)) 
                    damage = 0;

                const currentTime = Date.now();
                if (this.player1.canDealCritical(currentTime, controls.PlayerOneCriticalHitCombination)) {
                    damage = getCriticalDamage(firstFighter);
                    this.player1.criticalDealed(currentTime);
                }

                this.player2.receiveDamage(damage)
                this.updateIndicatorsHandler([this.player1.getHealthInPercent(), this.player2.getHealthInPercent()])
                this.log.push({
                    fighter1Shot: damage,
                    fighter2Shot: 0,
                    fighter1Health: this.player1.health,
                    fighter2Health: this.player2.health
                })
                break;
            }
            case players.SECOND: {
                this.player2.handleKeyPress(code);

                if (code === controls.PlayerTwoBlock) {
                    this.player2.setBlocking(true);
                    break;
                }

                if (code === controls.PlayerTwoAttack && this.player2.isBlocking() === false)
                    damage = getDamage(secondFighter, firstFighter);

                if (this.player1.isBlocking(controls.PlayerOneBlock)) 
                    damage = 0;

                const currentTime = Date.now();
                if (this.player2.canDealCritical(currentTime, controls.PlayerTwoCriticalHitCombination)) {
                    damage = getCriticalDamage(firstFighter)
                    this.player2.criticalDealed(currentTime)
                }

                this.player1.receiveDamage(damage)
                this.updateIndicatorsHandler([this.player1.getHealthInPercent(), this.player2.getHealthInPercent()])
                this.log.push({
                    fighter1Shot: 0,
                    fighter2Shot: damage,
                    fighter1Health: this.player1.health,
                    fighter2Health: this.player2.health
                })
                break;
            }
        }

        if (!this.player1.isAlive()) {
            this.fightFinishedHandler(players.SECOND, this.log);
        }
    
        if (!this.player2.isAlive()) {
            this.fightFinishedHandler(players.FIRST, this.log);
        }
    }
}

function decidePlayerByButton(keyCode) {
    if (keyCode === controls.PlayerOneAttack || 
        keyCode === controls.PlayerOneBlock || 
        controls.PlayerOneCriticalHitCombination.includes(keyCode))
        return players.FIRST;
    if (keyCode === controls.PlayerTwoAttack || 
        keyCode === controls.PlayerTwoBlock || 
        controls.PlayerTwoCriticalHitCombination.includes(keyCode))
        return players.SECOND;
}
  
function getDamage(attacker, defender) {
    // return damage
    return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}
  
function getCriticalDamage(fighter) {
    let power = fighter.power * 2;
    return power;
}
  
function getHitPower(fighter) {
    // return hit power
    let criticalHitChance = Math.random() + 1;
    let power = fighter.power * criticalHitChance;
    return power;
}
  
function getBlockPower(fighter) {
    // return block power
    let dodgeChance = Math.random() + 1;
    let power = fighter.defense * dodgeChance;
    return power;
}

export default Fight;