import React from 'react'

export function Fighter({ health, media }) {

    return (
        <div className="fighter-preview">
            <div className="health-indicator">
                <div className="health-bar" style={{width: `${health}%`}}></div>
            </div>
            <img src={media} />
        </div>
    )
}