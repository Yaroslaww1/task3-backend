import React from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'

export function FightLog({ fightLog }) {
    
    function getFightLogItems() {
        if (!fightLog || fightLog.length === 0)
            return []
        let items = []
        for (let log of fightLog) {
            if (log.fighter1Shot > 0) {
                items.push(
                    <ListItem >
                        <ListItemText className="first-player-damage-log" primary={`First player deal ${log.fighter1Shot} damage`}/>
                    </ListItem >
                )
            } else {
                items.push(
                    <ListItem >
                        <ListItemText className="second-player-damage-log" primary={`Second player deal ${log.fighter2Shot} damage`}/>
                    </ListItem >
                )
            }
        }
        let lastLog = fightLog[fightLog.length - 1]
        if (lastLog.fighter1Health > 0) {
            items.push(
                <ListItem >
                    <ListItemText className="victory-log" primary={`🏆 First player won 🏆`}/>
                </ListItem >
            )
        } else {
            items.push(
                <ListItem >
                    <ListItemText className="victory-log" primary={`🏆 Second player won 🏆`}/>
                </ListItem >
            )
        }
        return (
            <List>
                {items}
            </List>
        )
    }

    return (
        <>
            {getFightLogItems()}
        </>
    )
}