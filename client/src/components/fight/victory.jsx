import React from 'react'

const players = {
    FIRST: 0,
    SECOND: 1,
}

export function Victory({ winner }) {

    const getWinnerMessage = (winner) => {
        let winnerMessage = ''
        if (winner === players.FIRST)
            winnerMessage = 'FIRST'
        else 
            winnerMessage = 'SECOND'

        winnerMessage = winnerMessage + ' player win'

        return winnerMessage
    }

    return (
        <div className="victory-message">
            {getWinnerMessage(winner)}
        </div>
    )
}