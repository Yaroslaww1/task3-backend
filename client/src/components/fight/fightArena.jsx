import React, { useState, useEffect } from 'react'
import Fight from '../../services/fightService'
import {Fighter} from './fighterPreview'

export function FightArena({ fighter1, fighter2, fightFinishedHandler }) {

    const [healthIndicators, setHealthIndicators] = useState([100, 100])

    function keydownHandler() {

    }

    function keyupHandler() {

    }

    useEffect(() => {
        let fight = new Fight(fighter1, fighter2, setHealthIndicators, fightFinishedHandler)

        keydownHandler = fight.handleKeydown
        keyupHandler = fight.handleKeyup

        setUpListeners()
        return () => {
            setDownListeners()
        }
    }, [])

    const setUpListeners = () => {
        document.addEventListener('keyup', keyupHandler);

        document.addEventListener('keydown', keydownHandler)
    }

    const setDownListeners = () => {
        document.removeEventListener('keyup', keyupHandler); 

        document.removeEventListener('keydown', keydownHandler)
    }

    return (
        <>
            <div className="fight-wrapper">
                <Fighter health={healthIndicators[0]} media="https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif"/>
                <Fighter health={healthIndicators[1]} media="https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif"/>
            </div>
        </>
    )
}