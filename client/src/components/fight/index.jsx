import React from 'react';

import { getFighters } from '../../services/domainRequest/fightersRequest';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';

import './fight.css'

import { FightLog } from './fightLog'
import { FightHistory } from './fightHistory'
import { Victory } from './victory'
import { FightArena } from './fightArena'
import { createFight, getAllFights } from '../../services/domainRequest/fightRequest'
import { HowToPlay } from './howToPlay'

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        fightLog: [],
        fights: [],
        winner: null,
        isFightStarted: false,
        showHistory: false,
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if (fighters && !fighters.error) {
            this.setState({ fighters });
        }
    }

    onFightStart = () => {
        this.setState({ isFightStarted: true })
        this.setState({ showHistory: false })
    }

    displayHistory = async () => {
        const fights = await getAllFights()
        this.setState({ fights })
        this.setState({ showHistory: true })
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({ fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    fightFinishedHandler = async (winner, log) => {
        this.setState({winner})
        await createFight({
            fighter1ID: this.state.fighter1.id,
            fighter2ID: this.state.fighter2.id,
            log
        })
    }

    render() {
        const  { fighter1, fighter2 } = this.state;
        return (
            <div id="wrapper">
                <NewFighter onCreated={this.onCreate} />
                <div id="figh-wrapper">
                    <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={this.onFightStart} variant="contained" color="primary">Start Fight</Button>
                        <HowToPlay/>
                    </div>
                    <div className="btn-wrapper">
                        <Button onClick={this.displayHistory} variant="contained" color="primary">Show history</Button>
                    </div>
                    <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} />
                </div>
                {this.state.showHistory && 
                    <div className="fight-log-wrapper"> 
                        <FightHistory fights={this.state.fights}/>
                    </div>
                }
                {this.state.isFightStarted ?
                    this.state.winner === null ?
                        <FightArena fighter1={this.state.fighter1} fighter2={this.state.fighter2} fightFinishedHandler={this.fightFinishedHandler}/>
                        :
                        <Victory winner={this.state.winner}/>
                    :
                    <div className="fight-log-wrapper">
                        <FightLog fightLog={this.state.fightLog}/>
                    </div>
                }
            </div>
        );
    }
}

export default Fight;