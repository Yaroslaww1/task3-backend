import React, { useState } from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Collapse from '@material-ui/core/Collapse'

import { FightLog } from "./fightLog"

export function FightHistory({ fights }) {

    const [opens, setOpens] = useState([])

    const isOpen = (fight) => {
        return opens.includes(fight)
    }

    const toggleOpen = (fight) => {
        if (!opens.includes(fight)) {
            setOpens(opens.concat([fight]))
        } else {
            setOpens(opens.filter(item => item !== fight))
        }
    }
    
    function getFightsHistory() {
        if (!fights || fights.length === 0)
            return []
        let items = []
        for (let fight of fights) {
            items.push(
                <>
                    <ListItem button onClick={() => toggleOpen(fight)}>
                        {isOpen(fight) ? <div>Close</div> : <div>Open fight #{fight.id} log </div>}
                    </ListItem>
                    <Collapse in={isOpen(fight)} timeout="auto" unmountOnExit>
                        <FightLog fightLog={fight.log}/>
                    </Collapse>
                </>
            )
        }
        return (
            <List>
                {items}
            </List>
        )
    }

    return (
        <>
            {getFightsHistory()}
        </>
    )
}