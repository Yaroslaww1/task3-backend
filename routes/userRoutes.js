const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
	try {
		let queryResult = UserService.searchAll()
		res.data = { "users": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	}
}, responseMiddleware)

router.get('/:id', (req, res, next) => {
	try {
		let { id } = req.params
		if (!id)
			throw new Error('id param cannot be null')

		let queryResult = UserService.search({ id })
		res.data = { "user": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	}
}, responseMiddleware)

router.post('/', createUserValid, (req, res, next) => {
	try {
		if (res.err)
			throw res.err

		let user = req.user

		if (!user) 
			throw new Error('User must not be null')

		let queryResult = UserService.add(user)
		res.data = { "user": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	}
}, responseMiddleware)

router.put('/:id', updateUserValid, (req, res, next) => {
	try {
		if (res.err)
			throw res.err

		let { id } = req.params
		if (!id)
			throw new Error('id param cannot be null')

		let newUser = req.user
		
		if (!newUser) 
			throw new Error('Notning to update (new user field is empty)')

		let queryResult = UserService.update(id, newUser)
		if (!queryResult) {
			throw new Error('User with this id is not exist')
		}
		res.data = { "user": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	} 
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {
	try {
		let { id } = req.params
		if (!id)
			throw new Error('id param cannot be null')

		let queryResult = UserService.delete(id)
		res.data = { "user": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	}
}, responseMiddleware)

module.exports = router;