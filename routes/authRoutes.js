const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action
        let data = req.body
        let user = AuthService.login(data)
        res.data = user
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

module.exports = router