const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
	try {
		let queryResult = FighterService.searchAll()
		res.data = { "fighters": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	}
}, responseMiddleware)

router.get('/:id', (req, res, next) => {
	try {
		let { id } = req.params
		if (!id)
			throw new Error('id param cannot be null')

		let queryResult = FighterService.search({ id })
		res.data = { "fighter": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	}
}, responseMiddleware)

router.post('/', createFighterValid, (req, res, next) => {
	try {
		if (res.err)
			throw res.err

		let fighter = req.fighter

		if (!fighter) 
			throw new Error('Fighter must not be null')

		let queryResult = FighterService.add(fighter)
		res.data = { "fighter": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	}
}, responseMiddleware)

router.put('/:id', updateFighterValid, (req, res, next) => {
	try {
		if (res.err)
			throw res.err

		let { id } = req.params
		if (!id)
			throw new Error('id param cannot be null')

		let newFighter = req.fighter
		
		if (!newFighter) 
			throw new Error('Notning to update (new fighter field is empty)')

		let queryResult = FighterService.update(id, newFighter)
		if (!queryResult) {
			throw new Error('Fighter with this id is not exist')
		}
		res.data = { "fighter": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	} 
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {
	try {
		let { id } = req.params
		if (!id)
			throw new Error('id param cannot be null')

		let queryResult = FighterService.delete(id)
		console.log(queryResult)
		res.data = { "fighter": queryResult }
	} catch (err) {
		res.err = err
	} finally {
		next()
	}
}, responseMiddleware)

module.exports = router;