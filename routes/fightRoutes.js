const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFightValid } = require('../middlewares/fight.creation.middleware')

const router = Router();

router.post('/', createFightValid, (req, res, next) => {
    try {
        if (res.err)
            throw res.err

        let { log } = req.body
        if (!log) {
            res.data = { "log": null }
            next()
        }

        let { fighter1ID, fighter2ID } = req.body
        let queryResult = FightService.add(fighter1ID, fighter2ID, log)
        res.data = { "fight": queryResult }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.get('/', (req, res, next) => {
    try {
        let queryResult  = FightService.searchAll()
        res.data = { "fights": queryResult }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.get('/:id', (req, res, next) => {
    try {
        let { id } = req.params
        if (!id)
            throw new Error('id param cannot be null')
        let queryResult  = FightService.search({ id })
        res.data = { "fight": queryResult }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

module.exports = router;