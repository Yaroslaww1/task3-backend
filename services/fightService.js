const { FightRepository } = require('../repositories/fightRepository');

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

class FightService {
    searchAll() {
        const fights = FightRepository.getAll()
        if (!fights)
            return null
        return fights
    }

    search(search) {
        const item = FightRepository.getOne(search)
        if (!item) {
            return null
        }
        return item
    }

    add(fighter1ID, fighter2ID, log) {
        try {
            console.log(log)
            let fight = FightRepository.create({
                fighter1: fighter1ID,
                fighter2: fighter2ID,
                log
            })
            return fight
        } catch (err) {
            console.error(err)
            return null
        }
    }
}

module.exports = new FightService();