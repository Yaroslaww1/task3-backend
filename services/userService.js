const { UserRepository } = require('../repositories/userRepository');

class UserService {
    searchAll() {
        const users = UserRepository.getAll()
        if (!users)
            return null
        return users
    }

    search(search) {
        const item = UserRepository.getOne(search)
        if (!item) {
            return null
        }
        return item
    }

    add(_user) {
        const user = UserRepository.create(_user)
        if (!user)
            return null
        return user
    }

    update(idOfUserToUpdate, _newUser) {
        const oldUser = UserRepository.getOne({ id: idOfUserToUpdate })
        if (!oldUser)
            return null
        const newUser = UserRepository.update(idOfUserToUpdate, _newUser)
        if (!newUser)
            return null
        return newUser
    }

    delete(idOfUserToDelete) {
        const user = UserRepository.delete(idOfUserToDelete)
        if (!user || 
            (Array.isArray(user) && user.length === 0)
        )
            return null
        return user
    }
}

module.exports = new UserService();