const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    searchAll() {
        const fighters = FighterRepository.getAll();
        if (!fighters)
            return null
        return fighters;
    }

    search(search) {
        const item = FighterRepository.getOne(search)
        if (!item) {
            return null
        }
        return item
    }

    add(_fighter) {
        const fighter = FighterRepository.create(_fighter)
        if (!fighter)
            return null
        return fighter
    }

    update(idOfFighterToUpdate, _newFighter) {
        const oldFighter = FighterRepository.getOne({ id: idOfFighterToUpdate })
        if (!oldFighter)
            return null
        const newFighter = FighterRepository.update(idOfFighterToUpdate, _newFighter)
        if (!newFighter)
            return null
        return newFighter
    }

    delete(idOfFighterToDelete) {
        const fighter = FighterRepository.delete(idOfFighterToDelete)
        if (!fighter || 
            (Array.isArray(fighter) && fighter.length === 0)
        )
            return null
        return fighter
    }
}

module.exports = new FighterService();