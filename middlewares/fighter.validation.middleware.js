const { fighter } = require('../models/fighter');

function validatePower(power) {
    if (!parseInt(power) || isNaN(power))
        return false
    if (power >= 100 || power <= 0)
        return false
    return true
}

function validateDefense(defense) {
    if (!parseInt(defense) || isNaN(defense))
        return false
    if (defense > 10 || defense <= 1)
        return false
    return true
}

const createFighterValid = (req, res, next) => {
    const fighterFromRequest = req.body
    try {
        if (fighterFromRequest.id) {
            throw new Error('Fighter can\'t has id field')
        }
    
        //remove all properties that is not in model
        Object.keys(fighterFromRequest).map((property) => {
            if (!fighter.hasOwnProperty(property))
                throw new Error(`Fighter can't has ${property} field`)
        })
    
        //check if all properties is not null
        Object.keys(fighter).map((property) => {
            if (property !== 'id' && !fighterFromRequest[property]) {
                throw new Error(`Fighter ${property} field must not be empty`)
            }
        })
        
        const { power, defense } = fighterFromRequest
    
        if (!validatePower(power)) {
            throw new Error('Power must be from number from 1 to 100')
        }
    
        if (!validateDefense(defense)) {
            throw new Error('Defense must be from number from 1 to 10')
        }
    
        req.fighter = fighterFromRequest
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}

const updateFighterValid = (req, res, next) => {
    const fighterFromRequest = req.body
    try {
        if (fighterFromRequest.id) {
            throw new Error('Fighter can\'t has id field')
        }
    
        //remove all properties that is not in model
        Object.keys(fighterFromRequest).map((property) => {
            if (!fighter.hasOwnProperty(property))
                throw new Error(`Fighter can't has ${property} field`)
        })
    
        //check if all properties is not null
        // Object.keys(fighter).map((property) => {
        //     if (property !== 'id' && !fighterFromRequest[property]) {
        //         throw new Error(`Fighter ${property} field must not be empty`)
        //     }
        // })
        
        const { power, defense } = fighterFromRequest
    
        if (power && !validatePower(power)) {
            throw new Error('Power must be from number from 1 to 100')
        }
    
        if (defense && !validateDefense(defense)) {
            throw new Error('Defense must be from number from 1 to 10')
        }
    
        req.fighter = fighterFromRequest
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;