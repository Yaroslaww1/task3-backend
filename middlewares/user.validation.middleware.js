const { user } = require('../models/user');

function validateEmail(email) {
    let isValid = RegExp(
        /^[\w.+\-]+@gmail\.com$/
    ).test(email)
    return isValid
}

function validatePhoneNumber(phoneNumber) {
    let isValid = RegExp(
        /^\+380\d{3}\d{2}\d{2}\d{2}$/
    ).test(phoneNumber)
    return isValid
}

const createUserValid = (req, res, next) => {
    const userFromRequest = req.body
    try {
        if (userFromRequest.id) {
            throw new Error('User can\'t has id field')
        }
    
        //remove all properties that is not in model
        Object.keys(userFromRequest).map((property) => {
            if (!user.hasOwnProperty(property))
                throw new Error(`User can't has ${property} field`)
        })
    
        //check if all properties is not null
        Object.keys(user).map((property) => {
            if (property !== 'id' && !userFromRequest[property]) {
                throw new Error(`User ${property} field must not be empty`)
            }
        })
        
        const { email, phoneNumber, password } = userFromRequest
    
        if (!validateEmail(email)) {
            throw new Error('User must have gmail email')
        }
    
        if (!validatePhoneNumber(phoneNumber)) {
            throw new Error('User must have ukrainian phone number')
        }
    
        if (password.length < 3) {
            throw new Error('User must have password with more than 3 symbols')
        }
        
        req.user = userFromRequest
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}

const updateUserValid = (req, res, next) => {
    const userFromRequest = req.body
    try {
        if (userFromRequest.id) {
            throw new Error('User can\'t has id field')
        }
    
        //remove all properties that is not in model
        Object.keys(userFromRequest).map((property) => {
            if (!user.hasOwnProperty(property))
                throw new Error(`User can't has ${property} field`)
        })
    
        //check if all properties is not null
        // Object.keys(user).map((property) => {
        //     if (property !== 'id' && !userFromRequest[property]) {
        //         throw new Error(`User ${property} field must not be empty`)
        //     }
        // })
        
        const { email, phoneNumber, password } = userFromRequest
    
        if (email && !validateEmail(email)) {
            throw new Error('User must have gmail email')
        }
    
        if (phoneNumber && !validatePhoneNumber(phoneNumber)) {
            throw new Error('User must have ukrainian phone number')
        }
    
        if (password && password.length < 3) {
            throw new Error('User must have password with more than 3 symbols')
        }

        req.user = userFromRequest
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;