const { fight } = require('../models/fight')
const FighterService = require('../services/fighterService');

const createFightValid = (req, res, next) => {
    try {
        let { fighter1ID, fighter2ID } = req.body
        if (!fighter1ID || !fighter2ID)
            throw new Error('figher1 or fighter2 is not selected')
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}

exports.createFightValid = createFightValid;