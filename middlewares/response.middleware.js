function isResponseNotFound(responseObject) {
    //responseObject is empty: {} - so its post/put/delete request
    if (Object.keys(responseObject).length === 0 && responseObject.constructor === Object)
        return false
    //get all object values and check if some of them are null
    const isNull = Object.values(responseObject)
        .some((element) => !element)
    return isNull
}

function getNotFoundMessage(dataObj) {
    //Get all keys and than get the first key
    //Ex: { user: {...} } will return "user"
    const subject = Object.keys(dataObj)[0]
    const notFoundMessage = `${subject} not found`
    return notFoundMessage
}

function getData(dataObject) {
    const dataValues = Object.values(dataObject)
    //if empty object
    if (dataValues.length === 0)
        return {}
    //if only one property
    if (dataValues.length === 1)
        return dataValues[0]
    //if complex object (with two or more properties)
    return dataObject
}

const responseMiddleware = (req, res, next) => {
    try {
        if (!res.err) {
            if (res.data && !isResponseNotFound(res.data))
                res.status(200).json(getData(res.data))
            else
                res.status(404).json({ 
                    error: true,
                    message: getNotFoundMessage(res.data)
                })
        } else {
            res.status(400).json({ 
                error: true,
                message: res.err.toString(),
            })
        }
    } catch (err) {
        res.status(400).json({
            error: true,
            message: 'Something wrong during responding'
        })
    } finally {
        next();
    }
}

exports.responseMiddleware = responseMiddleware;